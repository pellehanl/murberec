LD B 100
LD A 0
LD C 0
LD G 0
Z1:
  INC C
  LD D 1
  LD F 0
  Z2:
    LD E C
    MOD C D
    LD L C
    LD C E
    INC D
    LD A D
    CP C
    RADD ZF L
    NOT ZF
    LD ZF π
    LD A 0
  JP Z2
  INC D
  LD A C
  CP D
  LD A 0
  NOT ZF
  RADD G π
  MUL π G
  MOV π C
  LD A C
  CP B
  LD A 0
JP Z1
