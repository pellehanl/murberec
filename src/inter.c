#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include <termios.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#define BF_MEM_SIZE 3000000

#define I 0
#define D 1
#define L 2
#define R 3
#define LO 4
#define LC 5
#define P 6
#define G 7

typedef signed int op;

typedef uint32_t I8;
typedef uint16_t I16;

typedef struct _state {
	int code_p;
	int mem_p;
	op *code;
	I8 *mem;
	int code_s;
} state;

typedef struct _stack {
	int val;
	struct _stack *next;
} stack;

/* push element onto stack (or init stack) */
void push(stack **stk, int val)
{
	stack *new = malloc(sizeof *new);
	new->val = val;
	new->next = *stk;
	*stk = new;
}
// +++[>++<-[>><<]]
/* pop element from stack */
int pop(stack **stk)
{
	assert(*stk);

	stack *tmp = *stk;
	int val = tmp->val;
	*stk = tmp->next;
	free(tmp);
	return val;
}

/* free all elements in stack */
void free_stack(stack *stk)
{
    while (stk) {
        stack *tmp = stk;
        stk = tmp->next;
        free(tmp);
    }
}

/* convert char to operator code */
op char_to_op(char c)
{
	switch (c) {
	case '+':
		return I;
		break;
	case '-':
		return D;
		break;
	case '>':
		return R;
		break;
	case '<':
		return L;
		break;
	case '[':
		return LO;
		break;
	case ']':
		return LC;
		break;
	case '.':
		return P;
		break;
	case ',':
		return G;
		break;
	default:
		return -1;
		break;
	}
}

/* initialize state */
state *init_st(char *code_str, int code_s)
{
	state *st = calloc(1, sizeof(state));
	st->code = calloc(code_s, sizeof(int));
	st->mem = calloc(BF_MEM_SIZE, sizeof(I8));

	stack *open = NULL;
	stack *close = NULL;
	int j = 0;

	/* store indices of opening brackets
	 * in closing brackets */
	for (int i = 0; i < code_s; ++i) {
		op o = char_to_op(code_str[i]);
		/* check if comment */
		if (o == -1) {
			continue;
		} else if (o == LO) {
			push(&open, j);
			st->code[j] = LO;
		} else if (o == LC) {
			st->code[j] = -pop(&open) - G;
		} else {
			st->code[j] = o;
		}
		++j;
	}
	free_stack(open);

	/* store indices of closing brackets
	 * in opening brackets */
	for (int i = j - 1; i >= 0; --i) {
		if (st->code[i] < 0)
			push(&close, i);
		else if (st->code[i] == LO)
			st->code[i] = pop(&close) + G;
	}

	free_stack(close);
	st->code_s = j;
	return st;
}

/* deallocate state */
void free_st(state *st)
{
	free(st->code);
	free(st->mem);
	free(st);
}

// + + + [ > + + [ ] < - ] 
//       *                

/* execute next command */
int step(state *st)
{
	op cmd = st->code[st->code_p];
	switch (cmd) {
	case I:
		st->mem[st->mem_p]++;
		break;
	case D:
		st->mem[st->mem_p]--;
		break;
	case R:
		st->mem_p++;
		break;
	case L:
		st->mem_p--;
		break;
	case P:
		fputc(st->mem[st->mem_p], stdout);
		break;
	case G:
		st->mem[st->mem_p] = fgetc(stdin);
		break;
	default:
		/* opening bracket */
		if (cmd > 0 && !st->mem[st->mem_p])
			st->code_p = cmd - G - 1;
		/* closing bracket */
		if (cmd < 0 && st->mem[st->mem_p])
			st->code_p = -cmd - G - 1;
		break;
	}
	st->code_p++;
	return st->code_p < st->code_s;
}

/* interpret brainfuck code */
void interpret(FILE *fp)
{
	fseek(fp, 0, SEEK_END);
	long fsize = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	char *code_str = malloc(fsize);
	fread(code_str, 1, fsize, fp);

	state *st = init_st(code_str, fsize - 1);
	free(code_str);

	unsigned long long int count = 0;

        while (step(st)) { ++count; }

        fflush(stdout);
	printf("\ntotal steps: %llu\n", count);

	free(st);
}


// TODO: implement: steps, backtrack n steps, breakpoints, memory viewer,
//       cursor highlighting, (loop coloring, last write color accordingly)
//       code

int ndig16(int n){return (int)(log(n)/log(0x10))+ 1;}

void print_mem_section(WINDOW* w, int ypos, int xpos, state* st, I16 addr){
    int _a = ndig16(addr+7*16) + 2;
    signed int addr_offset = addr - (addr % 16) - 3*16;
    mvwprintw(w, ypos++, xpos, "%.*s |  0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f", _a,"      ");
    mvwprintw(w, ypos++, xpos, "%.*s-+------------------------------------------------", _a,"------");
    
    for (int i = 0; i < 7; ++i) {
        if(addr_offset + 16*i < 0){
            mvwprintw(w, ypos+i, xpos, "%.*s |", _a, "######");
        }else{
            mvwprintw(w, ypos+i, xpos, "0x%.*x |", _a-2, addr_offset+16*i);
        }
        for (int j = 0; j < 16; ++j) {
            if(addr_offset + 16*i + j < 0){
                mvwprintw(w, ypos+i,xpos+j*3+_a+3, "##");
            }else{
                if(st->mem[addr_offset + 16*i + j]){
                mvwprintw(w, ypos+i,xpos+j*3+_a+3, "%x\n", st->mem[addr_offset + 16*i + j]);}
            }
        }
    }
    wrefresh(w);
}


/* handle command-line arguments and open file */
int main(int argc, char *argv[])
{
	if (argc != 2) {
		fprintf(stderr, "Usage: %s <file>\n", argv[0]);
		return EXIT_FAILURE;
	}

	FILE *fp = fopen(argv[1], "r");
	if (!fp) {
		fprintf(stderr, "Error: failed to open file '%s'\n", argv[1]);
		return EXIT_FAILURE;
	}

	//interpret(fp);
        int x,y;
        initscr();
        raw();
        noecho();
        start_color();
        getmaxyx(stdscr, y, x); //wtf
        //getchar();
        endwin();
        nonl();
        interpret(fp);
	fclose(fp);
	return EXIT_SUCCESS;
}
