module Mur where

import Text.ParserCombinators.Parsec
import Control.Monad
import Data.Char
import Data.Function
import Data.Functor

data Reg = U | V | W | Π | I | N | O | P | Q | R | S | T | A | B | C | D | E | F | G | H | L | HL | ZF | ZN | ZC | ZH | M | K deriving (Eq, Show, Read) -- M and K must stay at the end of the lookup, since they are the M=v_t, K=δ of the mock cell
data LOOP = LOOP [LOOP] | BSL | BSR | BINC | BDEC | BPUT | BREAD | REG Reg |
            RES Reg deriving (Eq, Show, Read)

regLkup :: [(Reg, Int)]
regLkup = zip [N,O,P,Q,R,S,T,U,V,W,A,B,C,D,E,F,G,H,L,I,ZF,ZN,ZC,ZH,Π,M,K][0..]

bsl :: Parser LOOP 
bsl = char '<' >> return BSL
bsr :: Parser LOOP
bsr = char '>' >> return BSR
binc :: Parser LOOP
binc = char '+' >> return BINC
bdec :: Parser LOOP
bdec = char '-' >> return BDEC
bput :: Parser LOOP
bput = char '.' >> return BPUT
bread :: Parser LOOP
bread = char ',' >> return BREAD

loop :: Parser LOOP
loop = char '[' >> LOOP <$> many (try loop <|> try op) <* char ']'

rmSpaces :: [Char] -> [Char]
rmSpaces = filter $ not . isSpace

op :: Parser LOOP
op = bsl <|> bsr <|> binc <|> bdec <|> bput <|> bread <|> reg <|> res

reg :: Parser LOOP
reg = do
    char '#'
    t <- many1 $ oneOf ('π':['A'..'Z'])
    let a = read (toUpper <$> t) :: Reg
    return $ REG a

res :: Parser LOOP
res = do
    char '~'
    t <- many1 $ oneOf ('π':['A'..'Z'])
    let a = read (toUpper <$> t) :: Reg
    return $ RES a

brainfuck :: Parser [LOOP]
brainfuck = many1 (op <|> loop)

fr :: [(Reg, Int)] -> Reg -> Int
fr l r = snd (head $ filter (\(x,y)->x==r) l)

delta :: Int -> Int -> String
delta i j | i < j = replicate (j-i) '>'
          | i > j = replicate (i-j) '<'
          | otherwise = ""

pretty :: (Int,[(Reg, Int)]) -> [LOOP] -> String
pretty (p,l) [] = ""
pretty (p,l) (BSL:xs)     = "<" ++ pretty (p-1,l) xs
pretty (p,l) (BSR:xs)     = ">" ++ pretty (p+1,l) xs
pretty (p,l) (BINC:xs)    = "+" ++ pretty (p,l) xs
pretty (p,l) (BDEC:xs)    = "-" ++ pretty (p,l) xs
pretty (p,l) (BPUT:xs)    = "." ++ pretty (p,l) xs 
pretty (p,l) (BREAD:xs)   = "," ++ pretty (p,l) xs
pretty (p,l) ((REG a):xs) = delta p (fr regLkup a) ++ pretty (fr regLkup a, l) xs
pretty (p,l) ((RES a):xs) = delta p (fr regLkup a) ++ "[-]" ++ delta (fr regLkup a) p ++ pretty (p,l) xs
pretty (p,l)((LOOP c):xs) =  "[" <> join (map (pretty (p,l)) [c]) <> "]" ++ pretty (p,l) xs

prettyprint x = reduce (pretty (0, regLkup) x)

------ "Optimizer" ------
split :: String -> [String]
split ('>':xs) = ('>' : takeWhile f xs) : split (dropWhile f xs) where
    f = \x -> x == '>' || x == '<'
split ('<':xs) = ('<' : takeWhile f xs) : split (dropWhile f xs) where
    f = \x -> x == '>' || x == '<'
split ('[':xs) = "[" : split xs
split (']':xs) = "]" : split xs
split (x:xs) = [x] : split xs
split "" = []

scnt:: (a -> Bool) -> [a] -> Int
scnt f (x:xs) = if f x then scnt f xs + 1 else scnt f xs - 1
scnt f [] = 0

s z | z > 0 = replicate z '>'
    | z < 0 = replicate (-z) '<'
    | otherwise = ""

rdce ('>':xs) = s (scnt ((==) '>') ('>' : xs))
rdce ('<':xs) = s (scnt ((==) '>') ('<' : xs))
rdce s = s

reduce s = join (rdce <$> split s)

