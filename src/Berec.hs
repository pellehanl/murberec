module Berec where
import Control.Monad
import Data.Functor
import Data.Function
import Data.Char
import Text.ParserCombinators.Parsec

import Mur hiding (op)
import Fucked

data OP = LD Reg Reg| ADD Reg | SUB Reg | SUBR Reg Reg | ADDR Reg Reg | MUL Reg Reg | CP Reg | JP Int | DEC Reg| AND Reg Reg | INC Reg | LDA Reg Int | GR Reg Reg | GREQ Reg Reg | MOD Reg Reg | MOV Reg Reg | NOT Reg | Loop [OP] deriving (Eq,Read,Show)

rreg :: Parser Reg
rreg = do
    x <- many1 $ oneOf ('π':['A'..'Z'])
    return $ read (toUpper <$> x)

int :: Parser Int
int = read <$> many1 digit

white :: Parser String
white = many $ char ' '
white1 :: Parser String
white1 = many1 $ char ' '

add :: Parser OP
add = white >> string "ADD" >> white1 >> ADD <$> rreg <* char '\n'
mul :: Parser OP
mul = white >> string "MUL" >> white1 >> MUL <$> rreg <* white1 <*> rreg <* char '\n'
sub :: Parser OP
sub = white >> string "SUB" >> white1 >> SUB <$> rreg <* char '\n'
cp  :: Parser OP
cp  = white >> string "CP"  >> white1 >> CP  <$> rreg <* char '\n'
ld  :: Parser OP
ld  = white >> string "LD"  >> white1 >> LD  <$> rreg <* white1 <*> rreg <* char '\n'
lda :: Parser OP
lda  = white >> string "LD" >> white1 >> LDA  <$> rreg <* white1 <*> int <* char '\n'
dec :: Parser OP
dec = white >> string "DEC" >> white1 >> DEC <$> rreg <* char '\n'
inc :: Parser OP
inc = white >> string "INC" >> white1 >> INC <$> rreg <* char '\n'
pmod :: Parser OP
pmod = white >> string "MOD" >> white1 >> MOD <$> rreg <* white1 <*> rreg <* char '\n'
mov :: Parser OP
mov = white >> string "MOV" >> white1 >> MOV <$> rreg <* white1 <*> rreg <* char '\n'
jp  :: Parser OP
jp  = white >> string "JP"  >> space >>  char 'Z' >> JP  <$> int <* char '\n'
mark :: Parser OP
mark = white >> char 'Z' >> int >> string ":\n" >> Loop <$> manyTill op jp
greq :: Parser OP
greq = white >> string "CPGEQ" >> white1 >> GREQ <$> rreg <* white1 <*> rreg <* char '\n'
pand :: Parser OP
pand = white >> string "AND" >> white1 >> AND <$> rreg <* white1 <*> rreg <* char '\n'
put :: Parser OP
put = undefined --white >> string "PUT" >> 
addr :: Parser OP
addr = white >> string "RADD" >> white1 >> ADDR <$> rreg <* white1 <*> rreg <* char '\n'
subr :: Parser OP
subr = white >> string "RSUB" >> white1 >> SUBR <$> rreg <* white1 <*> rreg <* char '\n'
pnot :: Parser OP
pnot = white >> string "NOT" >> white1 >> NOT <$> rreg <* char '\n'

op :: Parser OP
--op = try lda <|> add <|> sub <|> cp <|> ld <|> dec <|> inc <|> mark <|> try mul <|> try mov <|> pand <|> pmod <|> pnot <|> subr <|> jp 
op = try lda <|> add <|> sub <|> cp <|> ld <|> dec <|> inc <|> mark <|> try mul <|> try mov <|> pand <|> pmod <|> pnot <|> try subr <|> addr

assembly :: Parser [OP]
assembly = many op

phi :: [OP] -> [LOOP]
phi ((LDA r v):xs) = alda r v ++ phi xs
phi ((LD x y ):xs) = ald x y  ++ phi xs
phi ((SUB r  ):xs) = asub r   ++ phi xs
phi ((ADD r  ):xs) = aadd r   ++ phi xs
phi ((MUL r v):xs) = amul r v ++ phi xs
phi ((MOD r v):xs) = amod r v ++ phi xs
phi ((INC r  ):xs) = ainc r   ++ phi xs
phi ((DEC r  ):xs) = adec r   ++ phi xs
phi ((CP  r  ):xs) = acp r    ++ phi xs
phi ((AND r v):xs) = aand r v ++ phi xs
phi ((GR  r v):xs) = agr  r v ++ phi xs
phi ((MOV a v):xs) = amov a v ++ phi xs
phi ((NOT r  ):xs) = anot r   ++ phi xs
phi ((ADDR r v):xs)= aradd r v++ phi xs
phi ((SUBR r v):xs)= arsub r v++ phi xs
phi ((Loop l ):xs) = alda ZF 1 ++ REG ZF : LOOP (BDEC : REG A : phi l ++ [REG ZF]) : phi xs
phi [] = []

passem s = parse assembly "" (f s) where
    f = unlines.map (dropWhile (== ' ')).lines

compileF s = do
    str <- readFile s
    let Right a = passem str
    return $ prettyprint $ phi a
